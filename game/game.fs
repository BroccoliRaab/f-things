open System
open System.Web.UI.WebControls

type Item = 
    {
        Name: string;
        Description: string
    }


type Lock =
    {
        LockedDescription: string; 
        UnlockedDescription: string;
        Key: Item;
        mutable Locked: bool;
    }

type Exit =
    |Exit 
    |LockedExit of Lock
    |NoExit


type Interactive =
    {
        Name: string;
        Description: string;
        Recieve: Option<Item>
    }

type Side =
    {
        Description: string;
        Exit: Exit
    }

type Room = 
    {
        Description: string;
        Items: Item list;
        Interactives: Option<Interactive List>
        East: Side;
        West: Side;
        North: Side;
        South: Side
    }

type Target =
    |Side of Side
    |Interactive of Interactive
    |Item of Item

type Operation =
    |Investigate
    |Go

type Command =
    {
        Op: Operation;
        Target: Target
    }

let determineTarget tString =
    None

let determineOp opString =
    match opString with
        |"Investigate" -> Some(Investigate)
        |"Go"-> Some(Go)
        | _ -> None
let constructFromTarget target op =
    match op with
        |Some(op) -> Some({Op = op; Target = target})
        |None -> None
    
let constructCommand (input: String)  = 
    let ind = input.IndexOf(" ", 0, 1)
    let op = determineOp (input.Substring(0, ind))
    let targ = determineTarget ( input.Substring(ind+1))
    match targ with
        |Some(targ) -> constructFromTarget targ op
        |None -> None


let exeCommand(cmd) =
    match cmd with
        |Some(cmd) -> match cmd.Op with
            |Investigate -> 
                let 
                printfn "%s" cmd.Target<'a>.Description
        |None -> printfn "Unkown Command"

let describeRoom (room : Room) =
    let d = room.Description
    printfn "%s" d




    


